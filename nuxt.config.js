require("dotenv").config();

export default {
    target: 'static',

    head: {
        title: 'aone',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: ''}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ]
    },

    css: [],

    plugins: [],

    components: true,

    buildModules: [
        '@nuxtjs/tailwindcss',
        '@nuxtjs/dotenv',
        '@nuxtjs/moment'
    ],

    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/pwa',
        '@nuxt/content',
    ],

    publicRuntimeConfig: {
        axios: {
            baseURL: process.env.BASE_URL,
        },
    },

    privateRuntimeConfig: {
        apiSecret: process.env.API_SECRET,
    },

    content: {},

    build: {},

    generate: {
        fallback: true
    }
}
