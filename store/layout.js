import Vue from 'vue';

export const state = () => ({
    sideBar: false
});

export const actions = {
    toggleSideBar: ({commit}, payload) => commit('updateToggleSideBar', payload.status)
}

export const mutations = {
    updateToggleSideBar: (state, data) => Vue.set(state, 'sideBar', data)
}
